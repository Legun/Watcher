package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"time"

	"github.com/mholt/archiver"
)

func bash(command string) {

	cmd := exec.Command("sh", "-c", command)
	stderr, err := cmd.StderrPipe()
	if err != nil {
		log.Fatal(err)
	}

	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}

	slurp, _ := ioutil.ReadAll(stderr)
	if slurp != nil {
		log.Fatalf("%s\n", slurp)
		fmt.Printf("%s\n", slurp)
	}
	if err := cmd.Wait(); err != nil {
		fmt.Println(err)
	}
}

func main() {
	currentTime := time.Now()
	f, err := os.OpenFile(currentTime.Format("2006-01-02")+"-watcher-errors.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer f.Close()
	log.SetOutput(f)

	var path string
	path = "/tmp/"

	if _, err := os.Stat("/tmp/trovemat-latest.zip"); err == nil {

		log.Println("update from archive")
		fmt.Println("update from archive")

		if err := archiver.Zip.Open("/tmp/trovemat-latest.zip", path); err != nil {
			log.Fatal(err)
		}

		file, err := os.Open("/tmp/Release/filesnotfound.txt")
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()

		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			err := os.Remove(scanner.Text())
			if err != nil {
				log.Print(err)

			}
		}

		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}

		files := [2]string{"hash_detailed.txt", "filesnotfound.txt"}
		for n := 0; n < len(files); n++ {
			err := os.Remove(path + files[n])
			if err != nil {
				log.Fatal(err)

			}
		}

		bash("cp -rp /tmp/Release/* /opt/trovemat/")

		if err := os.Remove("/tmp/trovemat-latest.zip"); err != nil {
			if err != nil {
				log.Fatal(err)
			}
		}
		if err := os.Chmod("/opt/trovemat/*.sh", 0755); err != nil {
			log.Fatal(err)
		}
		if err := os.Chmod("/opt/trovemat/Kiosk", 0755); err != nil {
			log.Fatal(err)
		}

	} else {
		if _, err := os.Stat("/tmp/Release/Kiosk"); err == nil {
			fmt.Print("update binary")
			bash("cp -rp /tmp/Release/Kiosk /opt/trovemat/")
			if err := os.Remove("/tmp/Release"); err != nil {
				if err != nil {
					log.Fatal(err)
				}
			}
			if err := os.Chmod("/opt/trovemat/Kiosk", 0755); err != nil {
				log.Fatal(err)
			}

		} else {
			fmt.Println("Nothing Update")
		}

	}

}
